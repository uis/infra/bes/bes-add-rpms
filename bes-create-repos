#! /bin/bash

set -e

# Create a Bes repository (or rather collection of them) at each name
# in $@.  This script is designed to work correctly even if there is
# already a repository in a location.

ARCHES="x86_64 i586"

# Ensure that we don't damage any existing keys.
set -C

for BASE in "${@}"; do
    for ARCH in ${ARCHES}; do
	# Create standard directories used by bes-inst-rpms
	mkdir -p -- "${BASE}/${ARCH}/repodata"
	mkdir -p -- "${BASE}/${ARCH}/rpm/src"
	mkdir -p -- "${BASE}/${ARCH}/rpm/nosrc"
	mkdir -p -- "${BASE}/${ARCH}/rpm/noarch"
	mkdir -p -- "${BASE}/${ARCH}/rpm/${ARCH}"
	# [ -e to try to avoid spurious error messages.
	# noclobber and || true so that the race condition doesn't hurt.
	[ -e "${BASE}/${ARCH}/repodata/repomd.xml.key" ] ||
	    cat <<EOF > "${BASE}/${ARCH}/repodata/repomd.xml.key" || true
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.2.4 (GNU/Linux)

mQGiBEbgE18RBADcrR74DNkKCzasM7iCLnB72cNOkTqXPoT8UA1WCnxRSUADYyJu
tLQJ45FSSPVy3NfS0yXJPOBQ0NacllJhHnqEeaSdvuMbdGTIZbUu0i4ZSQzLkN+r
uDrXmyxlmXUecjfvM1nnBIzuYLC3qilQuogtZpcb5W4HGauzbwfvkTorjwCgnrs0
02g55JNKlKes+lfWkPn17MkD/j20DTnEpRHLpOcHFAJF4jcdk6rFhnmTyX9Id/1b
T0jioowawCVwcdBmqs7s1pvl7GbL3keYbloa1iNYB0wsUXkVDk2tz83VfTkG4QkI
kiYaWrUdmtTIoz9Yojw4IXQJa7r8iG6UBkBtzP4vOI6yghO/nn1FZm7pzU5V4I3m
AZSWBADPYVDA/w51f0MlGMpesi0n+4ZSyD+uHZiUeCgFtCR6lZAzu2pLg43nCPPb
4LPknI4Pwht00aZ0pE4uL4vrBnTp2oKBG2FsvxBEF61LdcGbgAednM/fP36aN2PY
IhdEjWNWhRQ3F2zRszodCqMDTf1m3nRcem20C9aRjZn8B6Ik57Q6VW5peCBTdXBw
b3J0IChSZXBvc2l0b3J5IEtleSkgPHVuaXgtc3VwcG9ydEB1Y3MuY2FtLmFjLnVr
PoheBBMRAgAeBQJG4BNfAhsDBgsJCAcDAgMVAgMDFgIBAh4BAheAAAoJEG2o2UUP
zt+d0YMAn0gNz04HVSaTOZiqV8Jz/tGngSYyAJ9/baF7RLlav+bcnj5I6aprSvol
OQ==
=yGxA
-----END PGP PUBLIC KEY BLOCK-----
EOF
	[ -e "${BASE}/${ARCH}/repodata/rpms.key" ] ||
	    cat <<EOF > "${BASE}/${ARCH}/repodata/rpms.key" || true
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1.2.4 (GNU/Linux)

mQGiBEbhElURBACda55h+BqZ0sGjidqq4TqmIM5IT4GdCyWxx3abjpeE1flsbIoQ
Lm0Rkra9syvZtYV8d5sHbVrHWvl1/bprIFHq73rqS3RdZ2CExuFlpRcS2IluIS4g
BIZMGd83ZiLWOgny2pKau3EXl/UgJau+o6bd/cVetNVmrqpfGB9tSwxkGwCgq+ut
T7cO+dFlhw+nJByiVodL+XkD/3VTZLa2dY/y6slBxPX4rUtUJYDvrAcQisuP5jlT
3WJ5krU3yjcc7h7xsozzbPMYCYyd+nu4kKnHNKk9CzI8Z/DGmlMXoufSTqDmz47a
IYB/qOoLJZTrcH0x8KkghQUp1DDcS77lpmKscrjP/yl1+v9yxsUQaWwF2olRrmwA
poxzA/9apWUhDLezXmEBR0tOEJbzHv4x8cOOqTEMpg4EyOAVpComGRsuh7DFDDvY
TYwpzWVFz1huJBhEHHlLf6aU2YTfSu1WjlXWUQISSYwgQ/Sg0M8PjvwSEmnhprZw
V49ool77WBUqhs9noVFcjuGFIG5dQTYVX0X18O+IsbfNxOANYrQ/VW5peCBTdXBw
b3J0IChLZXkgZm9yIFNMRVMxMCBSUE1zKSA8dW5peC1zdXBwb3J0QHVjcy5jYW0u
YWMudWs+iGQEExECACQFAkbhElUCGwMFCQ0oaIAGCwkIBwMCAxUCAwMWAgECHgEC
F4AACgkQTZSyMJYaQvYQYACeM6l9QUUojQxwzn2uugzgtIGZwXYAn2sgTPk0UU/l
veolZl9FXXgCvCi7
=seft
-----END PGP PUBLIC KEY BLOCK-----
EOF
    done
done
